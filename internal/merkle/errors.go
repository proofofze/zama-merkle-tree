package merkle

import (
	"github.com/juju/errors"
)

var (
	// ErrNoBlocksToCompute is returned when it's not possible to compute a merkle tree due to no blocks.
	ErrNoBlocksToCompute = errors.New("merkle: no blocks to compute merkle tree")
	// ErrNoLeafNodesFound is returned when there are no leaf nodes found.
	ErrNoLeafNodesFound = errors.New("merkle: no leaf nodes found")
	// ErrIndexOutOfBound is returned when the provided index is out of bound.
	ErrIndexOutOfBound = errors.New("merkle: index out of bound")
)
