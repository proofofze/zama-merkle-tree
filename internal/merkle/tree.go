package merkle

import (
	"math"
	"sync"

	"github.com/juju/errors"
)

// Hasher hashes data blocks.
type Hasher interface {
	Hash(block string) (string, error)
}

// Node is a Merkle Tree node.
type Node struct {
	Hash  string
	Left  *Node
	Right *Node
	Level int
}

// Tree is a Merkle tree with a branching factor of 2.
type Tree struct {
	root *Node

	blocks []string
	hasher Hasher

	hashers int
	inChan  chan hashInput
	outChan chan hashOutput
}

// NewTree initializes a new Merkle Tree.
func NewTree(hasher Hasher, hashNum int) *Tree {
	tree := &Tree{
		hasher: hasher,

		hashers: hashNum,
		inChan:  make(chan hashInput),
		outChan: make(chan hashOutput),
	}

	for i := 0; i < hashNum; i++ {
		go tree.startHasher()
	}

	return tree
}

// Reset cleans all nodes and blocks from the tree.
func (t *Tree) Reset() {
	t.root = nil
	t.blocks = make([]string, 0)
}

// RootHash gets the hash value of the root node.
func (t *Tree) RootHash() string {
	if t.root == nil {
		return ""
	}

	return t.root.Hash
}

// ValidateBlock validates a data block by its order and Merkle proof.
func (t *Tree) ValidateBlock(block string, index int, proof []string) (bool, error) {
	if t.root == nil {
		return false, nil
	}

	if index < 0 || index > len(t.blocks) {
		return false, ErrIndexOutOfBound
	}

	hash, err := t.hasher.Hash(block)
	if err != nil {
		return false, errors.Trace(err)
	}

	pos := index
	for _, p := range proof {
		if pos%2 == 0 {
			// left
			hash += p
		} else {
			// right
			hash = p + hash
		}

		hash, err = t.hasher.Hash(hash)
		if err != nil {
			return false, errors.Trace(err)
		}

		pos /= 2
	}

	return hash == t.root.Hash, nil
}

// ProofByIndex gets the Merkle proof/path from the index of the leaf nodes.
func (t *Tree) ProofByIndex(i int) ([]string, error) {
	if i < 0 {
		return nil, ErrIndexOutOfBound
	}

	if i >= len(t.blocks) {
		return nil, ErrIndexOutOfBound
	}

	level := t.root.Level - 1
	path := make([]string, 0, level)
	node := t.root
	middle := int(math.Pow(2, float64(level-1)))
	for level > 0 {
		level--

		nextLevelMiddle := int(math.Pow(2, float64(level-1)))

		if i >= middle {
			path = append(path, node.Left.Hash)
			node = node.Right
			middle += nextLevelMiddle

			continue
		}

		path = append(path, node.Right.Hash)
		node = node.Left
		middle -= nextLevelMiddle
	}

	leafToRootPath := make([]string, 0, len(path))
	for i := len(path) - 1; i >= 0; i-- {
		leafToRootPath = append(leafToRootPath, path[i])
	}

	return leafToRootPath, nil
}

// WriteBlocks writes data blocks on the tree. Data blocks are not hashed yet
// and still need to be Computed.
func (t *Tree) WriteBlocks(blocks ...string) {
	if blocks == nil {
		return
	}

	for _, block := range blocks {
		if block == "" {
			continue
		}

		t.blocks = append(t.blocks, block)
	}
}

// Compute computes the Tree from the raw written data blocks.
func (t *Tree) Compute() error {
	if len(t.blocks) == 0 {
		return ErrNoBlocksToCompute
	}

	leafNodes, err := t.buildLeafNodes(t.blocks...)
	if err != nil {
		return errors.Trace(err)
	}

	root, err := t.buildNode(leafNodes)
	if err != nil {
		return errors.Trace(err)
	}

	t.root = root

	return nil
}

// IncrementalCompute writes data blocks on the tree and computes an updated
// version of the Merkle Tree from the previous state.
//
// TODO: Delete Compute function and always use IncrementalCompute instead. Change
// it to accept a slice of blocks.
func (t *Tree) IncrementalCompute(block string) error {
	if t.root == nil || (t.root.Right == nil || t.root.Left == nil) {
		// tree not computed or contains only a leaf node
		t.WriteBlocks(block)

		return t.Compute()
	}

	hash, err := t.hasher.Hash(block)
	if err != nil {
		return errors.Trace(err)
	}

	leafNum := len(t.blocks)
	for i := 1; i <= leafNum; i *= 2 {
		if i != leafNum {
			continue
		}

		t.WriteBlocks(block)
		return t.buildRootFromUniqueHash(leafNum, hash)
	}

	lastNode := t.getLastNode(t.root)
	if lastNode == nil {
		return ErrNoLeafNodesFound
	}

	t.WriteBlocks(block)

	newRoot, err := t.incrementalCompute(t.root, hash)
	if err != nil {
		return errors.Trace(err)
	}

	t.root = newRoot

	return nil
}

func (t *Tree) buildNode(nodes []*Node) (*Node, error) {
	if len(nodes) == 0 {
		return &Node{}, nil
	}

	if len(nodes) == 1 {
		return nodes[0], nil
	}

	parentNodes := make([]*Node, 0, len(nodes)/2)
	for i := 0; i < len(nodes); i += 2 {
		var (
			leftNode  = nodes[i]
			rightNode = nodes[i]
		)

		if i < len(nodes)-1 {
			rightNode = nodes[i+1]
		}

		hash, err := t.hasher.Hash(leftNode.Hash + rightNode.Hash)
		if err != nil {
			return nil, errors.Trace(err)
		}

		parentNodes = append(parentNodes, &Node{
			Hash:  hash,
			Left:  leftNode,
			Right: rightNode,
			Level: leftNode.Level + 1,
		})
	}

	return t.buildNode(parentNodes)
}

// getLastNode returns the node on the tree that is further to the right.
// Note that leaf nodes are not returned.
func (t *Tree) getLastNode(node *Node) *Node {
	if node == nil {
		return nil
	}

	if node.Right == nil {
		return node
	}

	return t.getLastNode(node.Right)
}

func (t *Tree) buildLeafNodes(blocks ...string) ([]*Node, error) {
	size := len(blocks) / t.hashers
	hashers := t.hashers
	wg := sync.WaitGroup{}
	leafNodes := make([]*Node, len(blocks))
	var mainErr error

	if size == 0 {
		size = len(blocks)
		hashers = 1
	}

	for i := 0; i < hashers; i++ {
		start := size * i
		end := size * (i + 1)
		if end > len(blocks) {
			end = len(blocks)
		}

		batch := blocks[start:end]

		wg.Add(1)
		go func(start, end int, batch []string) {
			defer wg.Done()
			hashes := make([]*Node, 0, len(batch))
			for _, block := range batch {
				hash, err := t.hasher.Hash(block)
				if err != nil {
					mainErr = err
				}

				hashes = append(hashes, &Node{
					Hash:  hash,
					Left:  nil,
					Right: nil,
					Level: 1,
				})
			}

			copy(leafNodes[start:end], hashes)
		}(start, end, batch)
	}

	wg.Wait()

	if mainErr != nil {
		return nil, errors.Trace(mainErr)
	}

	return leafNodes, nil
}

func (t *Tree) incrementalCompute(node *Node, newHash string) (*Node, error) {
	if node == nil {
		return nil, ErrNoLeafNodesFound
	}

	if node.Right == nil {
		// leaf node
		return node, nil
	}

	if node.Right.Right == nil {
		// parent of leaf node
		leftNode := node.Left
		rightNode := node.Right
		if node.Left.Hash == node.Right.Hash {
			rightNode = &Node{
				Hash:  newHash,
				Level: 1,
			}
		}

		hash, err := t.hasher.Hash(leftNode.Hash + rightNode.Hash)
		if err != nil {
			return nil, errors.Trace(err)
		}

		return &Node{
			Hash:  hash,
			Left:  leftNode,
			Right: rightNode,
			Level: leftNode.Level + 1,
		}, nil
	}

	n, err := t.incrementalCompute(node.Right, newHash)
	if err != nil {
		return nil, errors.Trace(err)
	}

	hash, err := t.hasher.Hash(node.Left.Hash + n.Hash)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return &Node{
		Hash:  hash,
		Left:  node.Left,
		Right: n,
		Level: node.Left.Level + 1,
	}, nil
}

// buildRootFromUniqueHash builds a new tree from a unique hash.
//
// i.e. imagine the actual tree has 4 leaf nodes. This tree has 3 levels.
// If we add another leaf node, the tree will have 5 leaf nodes. However, a
// Merkle tree can't have 5 leaves but rather has to have 8 so it can have another
// level. This function builds another branch of 4 leaves and 3 levels so it can
// be added to the 4 initial leaves. All together will form a 8 leaves tree with
// 4 levels.
func (t *Tree) buildRootFromUniqueHash(leafNodesNum int, hash string) error {
	tree, err := t.buildBranchFromUniqueHash(leafNodesNum, hash)
	if err != nil {
		return errors.Trace(err)
	}

	hash, err = t.hasher.Hash(t.root.Hash + tree.Hash)
	if err != nil {
		return errors.Trace(err)
	}

	t.root = &Node{
		Hash:  hash,
		Left:  t.root,
		Right: tree,
		Level: t.root.Level + 1,
	}

	return nil
}

func (t *Tree) buildBranchFromUniqueHash(leafNodesNum int, hash string) (*Node, error) {
	leafNodes := make([]*Node, 0, leafNodesNum)
	for i := 0; i < leafNodesNum; i++ {
		leafNodes = append(leafNodes, &Node{
			Hash:  hash,
			Left:  nil,
			Right: nil,
			Level: 1,
		})
	}

	tree, err := t.buildNode(leafNodes)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return tree, nil
}

func (t *Tree) startHasher() {
	for {
		input := <-t.inChan

		hash, err := t.hasher.Hash(input.block)
		if err != nil {
			t.outChan <- hashOutput{
				i:   input.i,
				err: err,
			}
			continue
		}

		t.outChan <- hashOutput{
			i:    input.i,
			hash: hash,
		}
	}
}

type hashInput struct {
	i     int
	block string
}

type hashOutput struct {
	i    int
	hash string
	err  error
}
