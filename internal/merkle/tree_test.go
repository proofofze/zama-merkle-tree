package merkle

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTree_WriteBlocks(t *testing.T) {
	t.Run("append data", func(t *testing.T) {
		blocks := []string{"hello", "there"}

		tree := NewTree(nil, 1)

		assert.Nil(t, tree.blocks)

		tree.WriteBlocks(blocks...)

		assert.Len(t, tree.blocks, 2)
		assert.Equal(t, tree.blocks, blocks)
	})

	t.Run("empty append", func(t *testing.T) {
		tree := NewTree(nil, 1)

		assert.Nil(t, tree.blocks)

		tree.WriteBlocks("")

		assert.Nil(t, tree.blocks)
	})

	t.Run("nil append", func(t *testing.T) {
		tree := NewTree(nil, 1)

		assert.Nil(t, tree.blocks)

		tree.WriteBlocks()

		assert.Nil(t, tree.blocks)
	})
}

func TestTree_Compute(t *testing.T) {
	tests := []struct {
		name         string
		blocks       []string
		expectedHash string
		err          error
	}{
		{
			name:         "even_blocks",
			blocks:       []string{"b1", "b2", "b3", "b4"},
			expectedHash: "Tth1h2th3h4",
			err:          nil,
		},
		{
			name:         "odd_blocks",
			blocks:       []string{"b1", "b2", "b3"},
			expectedHash: "Tth1h2th3h3",
			err:          nil,
		},
		{
			name:         "odd_blocks_cascade_dup_hash",
			blocks:       []string{"b1", "b2", "b3", "b4", "b5"},
			expectedHash: "Ttth1h2th3h4tth5h5th5h5",
			err:          nil,
		},
		{
			name:         "one_block",
			blocks:       []string{"b1"},
			expectedHash: "h1",
			err:          nil,
		},
		{
			name:         "no_blocks",
			blocks:       []string{},
			expectedHash: "",
			err:          ErrNoBlocksToCompute,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tree := NewTree(&mockHasher{}, 10)

			tree.WriteBlocks(tt.blocks...)

			err := tree.Compute()
			if tt.err != nil {
				assert.Error(t, tt.err, err)
				return
			}

			assert.Nil(t, err)
			assert.Equal(t, tt.expectedHash, tree.root.Hash)
		})
	}
}

func TestTree_IncrementalCompute(t *testing.T) {
	testFields := []struct {
		newBlock         string
		expectedRootNode *Node
	}{
		{
			newBlock:         "b1",
			expectedRootNode: node11,
		},
		{
			newBlock:         "b2",
			expectedRootNode: node21,
		},
		{
			newBlock:         "b3",
			expectedRootNode: node31,
		},
		{
			newBlock:         "b4",
			expectedRootNode: node32,
		},
		{
			newBlock:         "b5",
			expectedRootNode: node41,
		},
	}

	tree := NewTree(&mockHasher{}, 10)
	for _, tFields := range testFields {
		err := tree.IncrementalCompute(tFields.newBlock)
		assert.Nil(t, err)
		assert.Equal(t, tFields.expectedRootNode, tree.root)
	}
}

func TestTree_ValidateBlock(t *testing.T) {
	testFields := []struct {
		name     string
		block    string
		index    int
		proof    []string
		expected bool
		err      error
	}{
		{
			name:  "error",
			block: "err",
			index: -1,
			err:   assert.AnError,
		},
		{
			name:  "index_out_of_bound",
			block: "b1",
			index: -1,
			proof: []string{"h2", "Th3h4", "Tth5h5th5h5"},
			err:   ErrIndexOutOfBound,
		},
		{
			name:     "first",
			block:    "b1",
			index:    0,
			proof:    []string{"h2", "Th3h4", "Tth5h5th5h5"},
			expected: true,
		},
		{
			name:     "second",
			block:    "b2",
			index:    1,
			proof:    []string{"h1", "Th3h4", "Tth5h5th5h5"},
			expected: true,
		},
		{
			name:     "fifth",
			block:    "b5",
			index:    5,
			proof:    []string{"h5", "Th5h5", "Tth1h2th3h4"},
			expected: true,
		},
		{
			name:     "invalid",
			block:    "b3",
			index:    1,
			proof:    []string{"h1", "Th3h4", "Tth5h5th5h5"},
			expected: false,
		},
		{
			name:  "index_out_of_bound",
			block: "b1",
			index: 9,
			proof: []string{"h2", "Th3h4", "Tth5h5th5h5"},
			err:   ErrIndexOutOfBound,
		},
	}

	tree := NewTree(&mockHasher{}, 10)
	tree.WriteBlocks([]string{"b1", "b2", "b3", "b4", "b5"}...)
	err := tree.Compute()
	assert.Nil(t, err)

	for _, tt := range testFields {
		t.Run(tt.name, func(t *testing.T) {
			ok, err := tree.ValidateBlock(tt.block, tt.index, tt.proof)
			if tt.err != nil {
				assert.Error(t, tt.err, err)
				return
			}

			assert.Nil(t, err)
			assert.Equal(t, tt.expected, ok)
		})
	}
}

func TestTree_PathByIndex(t *testing.T) {
	testFields := []struct {
		name         string
		index        int
		expectedPath []string
		err          error
	}{
		{
			name:  "index_out_of_bound",
			index: -1,
			err:   ErrIndexOutOfBound,
		},
		{
			name:         "first",
			index:        0,
			expectedPath: []string{"h2", "Th3h4", "Tth5h5th5h5"},
			err:          nil,
		},
		{
			name:         "second",
			index:        1,
			expectedPath: []string{"h1", "Th3h4", "Tth5h5th5h5"},
			err:          nil,
		},
		{
			name:         "third",
			index:        2,
			expectedPath: []string{"h4", "Th1h2", "Tth5h5th5h5"},
			err:          nil,
		},
		{
			name:         "fourth",
			index:        3,
			expectedPath: []string{"h3", "Th1h2", "Tth5h5th5h5"},
			err:          nil,
		},
		{
			name:         "fifth",
			index:        4,
			expectedPath: []string{"h5", "Th5h5", "Tth1h2th3h4"},
			err:          nil,
		},
		{
			name:  "index_out_of_bound",
			index: 5,
			err:   ErrIndexOutOfBound,
		},
	}

	tree := NewTree(&mockHasher{}, 10)
	tree.WriteBlocks([]string{"b1", "b2", "b3", "b4", "b5"}...)
	err := tree.Compute()
	assert.Nil(t, err)

	for _, tt := range testFields {
		t.Run(tt.name, func(t *testing.T) {
			path, err := tree.ProofByIndex(tt.index)
			if tt.err != nil {
				assert.Error(t, tt.err, err)
				return
			}

			assert.Nil(t, err)
			assert.Equal(t, tt.expectedPath, path)
		})
	}
}

type mockHasher struct{}

func (m mockHasher) Hash(block string) (string, error) {
	switch block {
	case "b1":
		return "h1", nil
	case "b2":
		return "h2", nil
	case "b3":
		return "h3", nil
	case "b4":
		return "h4", nil
	case "b5":
		return "h5", nil
	case "h1h2":
		return "Th1h2", nil
	case "h3h4":
		return "Th3h4", nil
	case "h3h3":
		return "Th3h3", nil
	case "h5h5":
		return "Th5h5", nil
	case "Th5h5Th5h5":
		return "Tth5h5th5h5", nil
	case "Th1h2Th3h3":
		return "Tth1h2th3h3", nil
	case "Th1h2Th3h4":
		return "Tth1h2th3h4", nil
	case "Tth1h2th3h4Tth5h5th5h5":
		return "Ttth1h2th3h4tth5h5th5h5", nil
	case "err":
		return "", assert.AnError
	}

	return "hello", nil
}

var (
	node11 = &Node{
		Hash:  "h1",
		Left:  nil,
		Right: nil,
		Level: 1,
	}
	node12 = &Node{
		Hash:  "h2",
		Left:  nil,
		Right: nil,
		Level: 1,
	}
	node13 = &Node{
		Hash:  "h3",
		Left:  nil,
		Right: nil,
		Level: 1,
	}
	node14 = &Node{
		Hash:  "h4",
		Left:  nil,
		Right: nil,
		Level: 1,
	}
	node15 = &Node{
		Hash:  "h5",
		Left:  nil,
		Right: nil,
		Level: 1,
	}

	node21 = &Node{
		Hash:  "Th1h2",
		Left:  node11,
		Right: node12,
		Level: 2,
	}
	node22 = &Node{
		Hash:  "Th3h3",
		Left:  node13,
		Right: node13,
		Level: 2,
	}
	node23 = &Node{
		Hash:  "Th3h4",
		Left:  node13,
		Right: node14,
		Level: 2,
	}
	node24 = &Node{
		Hash:  "Th5h5",
		Left:  node15,
		Right: node15,
		Level: 2,
	}

	node31 = &Node{
		Hash:  "Tth1h2th3h3",
		Left:  node21,
		Right: node22,
		Level: 3,
	}

	node32 = &Node{
		Hash:  "Tth1h2th3h4",
		Left:  node21,
		Right: node23,
		Level: 3,
	}

	node33 = &Node{
		Hash:  "Tth5h5th5h5",
		Left:  node24,
		Right: node24,
		Level: 3,
	}

	node41 = &Node{
		Hash:  "Ttth1h2th3h4tth5h5th5h5",
		Left:  node32,
		Right: node33,
		Level: 4,
	}
)
