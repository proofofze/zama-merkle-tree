package server

import (
	"context"
	"io"
	"lab/zama-merkle-tree/internal/upload"
	"lab/zama-merkle-tree/proto/server/pb"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
)

func TestServer_Upload(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		stream := &mockStream{
			ctx:          context.Background(),
			recvToServer: make(chan *pb.UploadRequest, 10),
		}
		tree := &mockMerkleTree{}
		server := NewServer(&mockFileUploader{}, tree)

		stream.recvToServer <- &pb.UploadRequest{
			File: &pb.File{
				Index: 0,
				Name:  "file1.txt",
				Data:  []byte("imafile"),
			},
		}
		close(stream.recvToServer)

		err := server.Upload(stream)
		assert.Nil(t, err)
		assert.Equal(t, []string{"imafile"}, tree.blocks)
	})

	t.Run("error_uploadMany", func(t *testing.T) {
		stream := &mockStream{
			ctx:          context.Background(),
			recvToServer: make(chan *pb.UploadRequest, 10),
		}
		server := NewServer(&mockFileUploader{}, &mockMerkleTree{})
		close(stream.recvToServer)
		err := server.Upload(stream)
		assert.ErrorIs(t, err, assert.AnError)
	})

	t.Run("error_merkle_compute", func(t *testing.T) {
		stream := &mockStream{
			ctx:          context.Background(),
			recvToServer: make(chan *pb.UploadRequest, 10),
		}
		server := NewServer(&mockFileUploader{}, &mockMerkleTree{err: assert.AnError})

		stream.recvToServer <- &pb.UploadRequest{
			File: &pb.File{
				Index: 0,
				Name:  "file1.txt",
				Data:  []byte("imafile"),
			},
		}
		close(stream.recvToServer)

		err := server.Upload(stream)
		assert.ErrorIs(t, err, assert.AnError)
	})
}

func TestServer_Download(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		tree := &mockMerkleTree{hash: "bigHash"}
		server := NewServer(&mockFileUploader{}, tree)

		resp, err := server.Download(context.Background(), &pb.DownloadRequest{Index: 11})
		assert.Nil(t, err)
		assert.Equal(t, &pb.DownloadResponse{
			File: &pb.File{
				Index: 11,
				Name:  "file.txt",
				Data:  []byte("imafile"),
			},
			Proof: []string{"hi"},
		}, resp)
	})

	t.Run("success_prewarm", func(t *testing.T) {
		server := NewServer(&mockFileUploader{}, &mockMerkleTree{})

		resp, err := server.Download(context.Background(), &pb.DownloadRequest{Index: 11})
		assert.Nil(t, err)
		assert.Equal(t, &pb.DownloadResponse{
			File: &pb.File{
				Index: 11,
				Name:  "file.txt",
				Data:  []byte("imafile"),
			},
			Proof: []string{"hi"},
		}, resp)
	})

	t.Run("error", func(t *testing.T) {
		tree := &mockMerkleTree{hash: "bigHash"}
		server := NewServer(&mockFileUploader{}, tree)

		_, err := server.Download(context.Background(), &pb.DownloadRequest{Index: 0})
		assert.ErrorIs(t, err, assert.AnError)
	})

	t.Run("error_index_out_of_bound", func(t *testing.T) {
		server := NewServer(&mockFileUploader{}, &mockMerkleTree{})

		_, err := server.Download(context.Background(), &pb.DownloadRequest{Index: -1})
		assert.ErrorIs(t, err, ErrInvalidIndex)
	})
}

func Test_fileValidation(t *testing.T) {
	tests := []struct {
		name string
		file *pb.File
		err  error
	}{
		{
			name: "valid",
			file: &pb.File{
				Index: 0,
				Name:  "hi.txt",
				Data:  []byte("hi"),
			},
		},
		{
			name: "empty_file",
			err:  ErrEmptyFile,
		},
		{
			name: "invalid_index",
			file: &pb.File{
				Index: -1,
			},
			err: ErrInvalidIndex,
		},
		{
			name: "no_data",
			file: &pb.File{
				Index: 0,
				Name:  "hi.txt",
				Data:  nil,
			},
			err: ErrEmptyFile,
		},
		{
			name: "no_data",
			file: &pb.File{
				Index: 0,
				Data:  []byte("hi"),
			},
			err: ErrNamelessFile,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := fileValidation(tt.file)
			if err != nil {
				assert.ErrorIs(t, err, tt.err)
				return
			}
			assert.Nil(t, err)
		})
	}
}

type mockFileUploader struct{}

func (m *mockFileUploader) UploadMany(_ context.Context, uploads []*upload.Upload) error {
	if uploads == nil {
		return assert.AnError
	}
	return nil
}

func (m *mockFileUploader) Download(_ context.Context, index int) (string, []byte, error) {
	if index == 11 {
		return "file.txt", []byte("imafile"), nil
	}
	return "", nil, assert.AnError
}

func (m *mockFileUploader) DownloadAll(_ context.Context) ([]*upload.Upload, error) {
	return []*upload.Upload{{
		Index: 0,
		Name:  "text.txt",
		Data:  []byte("imafile"),
	}}, nil
}

type mockMerkleTree struct {
	blocks []string
	hash   string
	err    error
}

func (m *mockMerkleTree) Compute() error {
	return m.err
}

func (m *mockMerkleTree) Reset() {}

func (m *mockMerkleTree) RootHash() string {
	return m.hash
}

func (m *mockMerkleTree) ProofByIndex(i int) ([]string, error) {
	if i == 11 {
		return []string{"hi"}, nil
	}
	return nil, nil
}

func (m *mockMerkleTree) WriteBlocks(blocks ...string) {
	m.blocks = append(m.blocks, blocks...)
}

type mockStream struct {
	grpc.ServerStream
	ctx          context.Context
	recvToServer chan *pb.UploadRequest
}

func (s *mockStream) SendAndClose(_ *pb.UploadResponse) error {
	return nil
}

func (s *mockStream) Recv() (*pb.UploadRequest, error) {
	req, more := <-s.recvToServer
	if !more {
		return nil, io.EOF
	}
	return req, nil
}

func (s *mockStream) Context() context.Context {
	return context.Background()
}
