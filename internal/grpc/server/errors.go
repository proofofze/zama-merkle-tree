package server

import (
	"github.com/juju/errors"
)

var (
	// ErrInvalidIndex is returned when the profided index is invalid.
	ErrInvalidIndex = errors.New("server: invalid index")
	// ErrNamelessFile is returned when the provided file has no nade.
	ErrNamelessFile = errors.New("server: file has no name")
	// ErrEmptyFile is returned when the provided file is empty.
	ErrEmptyFile = errors.New("server: file is empty")
)
