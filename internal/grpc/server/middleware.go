package server

import (
	"context"
	"log"

	"google.golang.org/grpc"
)

// LoggingMiddleware logs errors for the grpc server.
type LoggingMiddleware struct {
	logger *log.Logger
}

// NewLoggingMiddleware instantiates a new LoggingMiddleware.
func NewLoggingMiddleware(logger *log.Logger) *LoggingMiddleware {
	return &LoggingMiddleware{
		logger: logger,
	}
}

// StreamInterceptor is the logging interceptor for the streaming endpoints.
func (m *LoggingMiddleware) StreamInterceptor(
	srv any,
	stream grpc.ServerStream,
	info *grpc.StreamServerInfo,
	handler grpc.StreamHandler,
) error {
	m.logger.Println(info.FullMethod)
	err := handler(srv, stream)
	if err != nil {
		m.logger.Println(err)
	}

	return err
}

// UnaryInterceptor is the logging interface for the unary endpoints.
func (m *LoggingMiddleware) UnaryInterceptor(
	ctx context.Context,
	req any,
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (any, error) {
	m.logger.Println(info.FullMethod)
	resp, err := handler(ctx, req)
	if err != nil {
		m.logger.Println(err)
	}

	return resp, err
}
