//go:generate mockgen -source=../../../proto/server/pb/server_grpc.pb.go -package=server -destination=server_mock.go FileUploaderServer
package server

import (
	"context"
	"fmt"
	"io"
	"time"

	"lab/zama-merkle-tree/internal/upload"
	"lab/zama-merkle-tree/proto/server/pb"

	"github.com/juju/errors"
)

// FileUploader uploads files.
type FileUploader interface {
	// UploadMany pushes uploads.
	UploadMany(ctx context.Context, uploads []*upload.Upload) error
	// Download fetches uploaded files.
	Download(ctx context.Context, index int) (string, []byte, error)
	// DownloadAll gets all files.
	DownloadAll(ctx context.Context) ([]*upload.Upload, error)
}

// MerkleTree is a merkle tree with a branching factor of 2.
type MerkleTree interface {
	// Reset resets the merkle tree.
	Reset()
	// RootHash returns the hash value of the root node.
	RootHash() string
	// ProofByIndex gets the Merkle proof/path from the index of the leaf nodes.
	ProofByIndex(i int) ([]string, error)
	// WriteBlocks writes data blocks on the tree. Data blocks are not hashed yet
	// and still need to be Computed.
	WriteBlocks(blocks ...string)
	// Compute computes the Tree from the raw written data blocks.
	Compute() error
}

// Server is the grpc API for this challenge.
type Server struct {
	pb.UnimplementedFileUploaderServer

	fileUploader FileUploader
	merkleTree   MerkleTree
}

// NewServer instantiates a new server.
func NewServer(fileUploader FileUploader, tree MerkleTree) *Server {
	return &Server{
		fileUploader: fileUploader,
		merkleTree:   tree,
	}
}

// Upload overrides old uploads.
func (s *Server) Upload(stream pb.FileUploader_UploadServer) error {
	var (
		ctx        = stream.Context()
		uploads    []*upload.Upload
		dataBlocks []string
	)

	for {
		req, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			return errors.Trace(err)
		}

		file := req.File
		err = fileValidation(file)
		if err != nil {
			return errors.Trace(err)
		}

		uploads = append(uploads, &upload.Upload{
			Index: int(file.Index),
			Name:  file.Name,
			Data:  file.Data,
		})
		dataBlocks = append(dataBlocks, string(file.Data))
	}

	err := s.fileUploader.UploadMany(ctx, uploads)
	if err != nil {
		return errors.Trace(err)
	}

	s.merkleTree.Reset()
	s.merkleTree.WriteBlocks(dataBlocks...)

	now := time.Now()
	err = s.merkleTree.Compute()
	if err != nil {
		return errors.Trace(err)
	}

	after := time.Now()
	fmt.Printf("merkle tree computation on upload took %s\n", after.Sub(now).String())

	return errors.Trace(stream.SendAndClose(&pb.UploadResponse{}))
}

// Download fetches files and the respective Merkle proofs.
func (s *Server) Download(ctx context.Context, request *pb.DownloadRequest) (*pb.DownloadResponse, error) {
	index := request.Index
	if index < 0 {
		return nil, ErrInvalidIndex
	}

	if s.merkleTree.RootHash() == "" {
		err := s.preWarm(ctx)
		if err != nil {
			return nil, errors.Trace(err)
		}
	}

	name, data, err := s.fileUploader.Download(ctx, int(request.Index))
	if err != nil {
		return nil, errors.Trace(err)
	}

	path, err := s.merkleTree.ProofByIndex(int(index))
	if err != nil {
		return nil, errors.Trace(err)
	}

	return &pb.DownloadResponse{
		File: &pb.File{
			Index: index,
			Name:  name,
			Data:  data,
		},
		Proof: path,
	}, nil
}

// preWarm gets all files from the uploader so it can build a merkle tree before
// the client starts to ask for the files.
func (s *Server) preWarm(ctx context.Context) error {
	uploads, err := s.fileUploader.DownloadAll(ctx)
	if err != nil {
		return errors.Trace(err)
	}

	now := time.Now()

	for _, u := range uploads {
		s.merkleTree.WriteBlocks(string(u.Data))
	}

	after := time.Now()
	fmt.Printf("merkle tree computation on prewarm took %s\n", after.Sub(now).String())

	return errors.Trace(s.merkleTree.Compute())
}

func fileValidation(f *pb.File) error {
	if f == nil {
		return ErrEmptyFile
	}

	if f.Index < 0 {
		return ErrInvalidIndex
	}

	if len(f.Data) == 0 {
		return ErrEmptyFile
	}

	if f.Name == "" {
		return ErrNamelessFile
	}

	return nil
}
