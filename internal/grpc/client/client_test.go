package client

import (
	"context"
	"lab/zama-merkle-tree/internal/file"
	"lab/zama-merkle-tree/internal/grpc/server"
	"lab/zama-merkle-tree/proto/server/pb"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/metadata"
)

func TestClient_UploadFiles(t *testing.T) {
	tController := gomock.NewController(t)
	grpcClient := server.NewMockFileUploaderClient(tController)

	t.Run("success", func(t *testing.T) {
		files := []*file.File{
			{
				Index: 0,
				Name:  "text.txt",
				Data:  []byte("hi"),
			},
		}

		grpcClient.EXPECT().Upload(
			gomock.Any(),
		).Return(&mockStream{}, nil)

		client := New(grpcClient)

		err := client.UploadFiles(context.Background(), files)
		assert.Nil(t, err)
	})

	t.Run("error_send", func(t *testing.T) {
		files := []*file.File{
			{
				Index: 1,
			},
		}

		grpcClient.EXPECT().Upload(
			gomock.Any(),
		).Return(&mockStream{}, nil)

		client := New(grpcClient)

		err := client.UploadFiles(context.Background(), files)
		assert.ErrorIs(t, err, assert.AnError)
	})

	t.Run("error_close", func(t *testing.T) {
		files := []*file.File{
			{
				Index: 0,
				Name:  "text.txt",
				Data:  []byte("hi"),
			},
		}

		grpcClient.EXPECT().Upload(
			gomock.Any(),
		).Return(&mockStream{err: assert.AnError}, nil)

		client := New(grpcClient)

		err := client.UploadFiles(context.Background(), files)
		assert.ErrorIs(t, err, assert.AnError)
	})
}

func TestClient_DownloadFile(t *testing.T) {
	tController := gomock.NewController(t)
	grpcClient := server.NewMockFileUploaderClient(tController)

	t.Run("success", func(t *testing.T) {
		grpcClient.EXPECT().Download(
			gomock.Any(),
			gomock.Eq(&pb.DownloadRequest{Index: 0}),
		).Return(&pb.DownloadResponse{
			File: &pb.File{
				Index: 0,
				Name:  "text.txt",
				Data:  []byte("hellothere"),
			},
			Proof: []string{"bigPath"},
		}, nil)

		client := New(grpcClient)

		f, proof, err := client.DownloadFile(context.Background(), 0)
		assert.Nil(t, err)
		assert.Equal(t, &file.File{
			Index: 0,
			Name:  "text.txt",
			Data:  []byte("hellothere"),
		}, f)
		assert.Equal(t, []string{"bigPath"}, proof)
	})

	t.Run("error", func(t *testing.T) {
		grpcClient.EXPECT().Download(
			gomock.Any(),
			gomock.Eq(&pb.DownloadRequest{Index: 0}),
		).Return(nil, assert.AnError)

		client := New(grpcClient)

		_, _, err := client.DownloadFile(context.Background(), 0)
		assert.ErrorIs(t, err, assert.AnError)
	})
}

type mockStream struct {
	err error
}

func (s *mockStream) Send(request *pb.UploadRequest) error {
	if request.File.Index != 0 {
		return assert.AnError
	}

	return nil
}

func (s *mockStream) CloseAndRecv() (*pb.UploadResponse, error) {
	return nil, s.err
}

func (s *mockStream) Header() (metadata.MD, error) {
	return nil, nil
}

func (s *mockStream) Trailer() metadata.MD {
	return nil
}

func (s *mockStream) CloseSend() error {
	return nil
}

func (s *mockStream) Context() context.Context {
	return nil
}

func (s *mockStream) SendMsg(_ any) error {
	return nil
}

func (s *mockStream) RecvMsg(_ any) error {
	return nil
}
