package client

import (
	"context"
	"io"
	"lab/zama-merkle-tree/internal/file"
	"lab/zama-merkle-tree/proto/server/pb"

	"github.com/juju/errors"
)

// Client works as the grpc Client to connect to the grpc server.
type Client struct {
	client pb.FileUploaderClient
}

// New instantiates a new client.
func New(client pb.FileUploaderClient) *Client {
	return &Client{
		client: client,
	}
}

// UploadFiles uploads files to the server.
func (c *Client) UploadFiles(ctx context.Context, files []*file.File) error {
	stream, err := c.client.Upload(ctx)
	if err != nil {
		return errors.Trace(err)
	}

	for _, f := range files {
		err := stream.Send(&pb.UploadRequest{
			File: &pb.File{
				Index: int64(f.Index),
				Name:  f.Name,
				Data:  f.Data,
			},
		})
		if err != nil {
			return errors.Trace(err)
		}
	}

	_, err = stream.CloseAndRecv()
	if err != nil && !errors.Is(err, io.EOF) {
		return errors.Trace(err)
	}

	return nil
}

// DownloadFile downloads a file with the index from the server.
func (c *Client) DownloadFile(ctx context.Context, index int) (*file.File, []string, error) {
	response, err := c.client.Download(ctx, &pb.DownloadRequest{Index: int64(index)})
	if err != nil {
		return nil, nil, errors.Trace(err)
	}

	return &file.File{
		Index: int(response.File.Index),
		Name:  response.File.Name,
		Data:  response.File.Data,
	}, response.Proof, nil
}
