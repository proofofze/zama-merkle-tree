package hash

import (
	"crypto/sha256"
	"encoding/hex"

	"github.com/juju/errors"
)

// SHA256Hasher hashes data blocks with SHA-256 hash function.
type SHA256Hasher struct{}

// NewSHA256Hasher instantiates a new SHA256Hasher.
func NewSHA256Hasher() SHA256Hasher {
	return SHA256Hasher{}
}

// Hash hashes a data block.
func (h SHA256Hasher) Hash(data string) (string, error) {
	hash := sha256.New()

	_, err := hash.Write([]byte(data))
	if err != nil {
		return "", errors.Trace(err)
	}

	return hex.EncodeToString(hash.Sum(nil)), nil
}
