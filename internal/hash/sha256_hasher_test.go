package hash

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSHA256Hasher_Hash(t *testing.T) {
	hasher := NewSHA256Hasher()

	t.Run("success", func(t *testing.T) {
		h, err := hasher.Hash("hello")
		assert.Nil(t, err)
		assert.Equal(t, "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824", h)
	})

	t.Run("empty data", func(t *testing.T) {
		h, err := hasher.Hash("")
		assert.Nil(t, err)
		assert.Equal(t, "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", h)
	})
}
