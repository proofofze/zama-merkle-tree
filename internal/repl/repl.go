package repl

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"lab/zama-merkle-tree/internal/file"
	"lab/zama-merkle-tree/internal/grpc/client"

	"github.com/juju/errors"
)

const (
	whitespaceSeparator = " "
	newlineSeparator    = "\n"
	lineFeedSeparator   = "\r"
)

type FileManager interface {
	Create(dir, name string, data []byte) error

	GetAll(dir string) ([]*file.File, error)
	RemoveAll(dir string) error

	DirExists(dir string) error
}

// MerkleTree is a merkle tree with a branching factor of 2.
type MerkleTree interface {
	// Reset resets the merkle tree.
	Reset()
	// RootHash returns the hash value of the root node.
	RootHash() string
	// ValidateBlock validates a data block by its order and Merkle proof.
	ValidateBlock(block string, index int, proof []string) (bool, error)
	// WriteBlocks writes data blocks on the tree. Data blocks are not hashed yet
	// and still need to be Computed.
	WriteBlocks(blocks ...string)
	// Compute computes the Tree from the raw written data blocks.
	Compute() error
}

type REPL struct {
	log *log.Logger

	grpcClient  *client.Client
	merkleTree  MerkleTree
	fileManager FileManager

	dir                 string
	lastComputeDuration time.Duration
}

func NewREPL(logger *log.Logger, grpcClient *client.Client, merkleTree MerkleTree, fileManager FileManager) *REPL {
	return &REPL{
		log:         logger,
		grpcClient:  grpcClient,
		merkleTree:  merkleTree,
		fileManager: fileManager,
	}
}

func (r *REPL) Start() error {
	fmt.Println("Hello! Welcome to the Merkle tree REPL")
	fmt.Println("Start writing your commands...")

	reader := bufio.NewReader(os.Stdin)
	for {
		input, err := r.readInput(reader)
		if err != nil {
			return errors.Trace(err)
		}

		output, err := r.evaluateInput(input)
		if action, ok := output.(Action); ok && action == QUIT {
			break
		}
		r.print(output, err)
	}

	return nil
}

func (r *REPL) readInput(reader *bufio.Reader) ([]string, error) {
	fmt.Print("> ")
	insertedString, err := reader.ReadString('\n')
	if err != nil {
		return nil, errors.Trace(err)
	}

	// remove carriage returns \n and line feeds \r.
	insertedString = strings.ReplaceAll(insertedString, newlineSeparator, "")
	insertedString = strings.ReplaceAll(insertedString, lineFeedSeparator, "")

	insertedInput := strings.Split(insertedString, whitespaceSeparator)
	insertedInput[0] = strings.ToUpper(insertedInput[0])
	return insertedInput, nil
}

func (r *REPL) evaluateInput(input []string) (interface{}, error) {
	action := NewAction(input[0])
	switch action {
	case DIR:
		return r.executeDir(input)
	case UPLOAD:
		return r.executeUpload()
	case DOWNLOAD:
		return nil, r.executeDownload(input)
	case ROOTHASH:
		return r.executeRootHash(), nil
	case COMPUTETIME:
		return r.executeComputeTime()
	case QUIT, EXIT:
		return QUIT, nil
	case HELP:
		return nil, r.executeHelp()
	default:
		return nil, ErrCommandNotFound
	}
}

func (r *REPL) print(printValue interface{}, err error) {
	if err != nil {
		r.log.Println(err)
		return
	}
	if printValue == nil {
		return
	}
	fmt.Println(printValue)
}

func (r *REPL) executeDir(input []string) (any, error) {
	if len(input) <= 1 {
		return nil, ErrInvalidInput
	}

	err := r.fileManager.DirExists(input[1])
	if err != nil {
		return "", errors.Trace(err)
	}

	r.dir = input[1]

	return fmt.Sprintf("dir changed to %s", input[1]), nil
}

func (r *REPL) executeUpload() (any, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	if r.dir == "" {
		return nil, ErrDirNoSet
	}

	files, err := r.fileManager.GetAll(r.dir)
	if err != nil {
		return nil, errors.Trace(err)
	}

	r.merkleTree.Reset()

	for _, f := range files {
		r.merkleTree.WriteBlocks(string(f.Data))
	}

	now := time.Now()

	err = r.merkleTree.Compute()
	if err != nil {
		return nil, errors.Trace(err)
	}

	after := time.Now()

	r.lastComputeDuration = after.Sub(now)

	err = r.grpcClient.UploadFiles(ctx, files)
	if err != nil {
		return nil, errors.Trace(err)
	}

	err = r.fileManager.RemoveAll(r.dir)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return fmt.Sprintf("computed merkle root hash is: %s", r.merkleTree.RootHash()), nil
}

func (r *REPL) executeDownload(input []string) error {
	if len(input) <= 1 {
		return ErrInvalidInput
	}

	if r.dir == "" {
		return ErrDirNoSet
	}

	if r.merkleTree.RootHash() == "" {
		return ErrRootHashNotBuild
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*2)
	defer cancel()

	index, err := strconv.ParseInt(input[1], 10, 64)
	if err != nil {
		return errors.Trace(err)
	}

	df, merkleProof, err := r.grpcClient.DownloadFile(ctx, int(index))
	if err != nil {
		return errors.Trace(err)
	}

	ok, err := r.merkleTree.ValidateBlock(string(df.Data), df.Index, merkleProof)
	if err != nil {
		return errors.Trace(err)
	}

	if !ok {
		return ErrInvalidFile
	}

	return errors.Trace(r.fileManager.Create(r.dir, df.Name, df.Data))
}

func (r *REPL) executeRootHash() any {
	return r.merkleTree.RootHash()
}

func (r *REPL) executeComputeTime() (any, error) {
	if r.lastComputeDuration == 0 {
		return "", ErrNoLastComputeDurationYet
	}

	return r.lastComputeDuration.String(), nil
}

func (r *REPL) executeHelp() error {
	fmt.Print(`    DIR <dir> - Pick a directory to upload all its files to the server. Downloaded files will also be added there.
    * UPLOAD - Uploads all files that are inside the picked directory. After, a root hash of the merkle tree computed from the uploaded files is saved. You can check the value with the ROOTHASH command.
    * DOWNLOAD <fileIndex> - Download the uploaded file to picked directory with the index <fileIndex> and validates it using the root hash saved during the upload and the merkle proof sent from the DOWNLOAD response.
    * ROOTHASH - Shows the hash of the merkle tree root node computed during the upload.
	* COMPUTETIME - Shows the duration of the last merkle tree computation.
    * HELP - Shows all available commands.
    * QUIT - Exit the REPL cleanly. A message to stderr may be output.
`)
	return nil
}
