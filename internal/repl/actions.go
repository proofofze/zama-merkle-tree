package repl

type Action string

const (
	DIR         Action = "DIR"
	UPLOAD      Action = "UPLOAD"
	DOWNLOAD    Action = "DOWNLOAD"
	ROOTHASH    Action = "ROOTHASH"
	COMPUTETIME Action = "COMPUTETIME"
	HELP        Action = "HELP"
	QUIT        Action = "QUIT"
	EXIT        Action = "EXIT"
)

// NewAction instantiates a new REPL action.
func NewAction(action string) Action {
	switch action {
	case "DIR":
		return DIR
	case "UPLOAD":
		return UPLOAD
	case "DOWNLOAD":
		return DOWNLOAD
	case "ROOTHASH":
		return ROOTHASH
	case "COMPUTETIME", "TIME", "CT":
		return COMPUTETIME
	case "HELP":
		return HELP
	case "QUIT":
		return QUIT
	case "EXIT":
		return EXIT
	}
	return "unknown"
}
