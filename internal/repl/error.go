package repl

import (
	"github.com/juju/errors"
)

var (
	// ErrInvalidInput is returned when the inserted input is invalid.
	ErrInvalidInput = errors.New("repl: invalid input for the action")
	// ErrInvalidFile is returned when a file was not correctly validated by the merkle proof. file is invalid or corrupted.
	ErrInvalidFile = errors.New("repl: invalid or corrupted file validated by merkle proof")
	// ErrCommandNotFound is returned when the inserted ommand is not found.
	ErrCommandNotFound = errors.New("repl: command not found. type HELP to see a list of all available commands")
	// ErrDirNoSet is returned when a directory was not chosen yet.
	ErrDirNoSet = errors.New("repl: dir not set yet")
	// ErrRootHashNotBuild is returned when the root hash is not built yet.
	ErrRootHashNotBuild = errors.New("repl: root hash not built. upload files first")
	// ErrNoLastComputeDurationYet is returned when no merkle tree computation was performed yet.
	ErrNoLastComputeDurationYet = errors.New("repl: no saved compute duration yet. upload files first")
)
