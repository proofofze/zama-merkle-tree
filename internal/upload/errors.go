package upload

import (
	"github.com/juju/errors"
)

// ErrDownloadingCorruptedUpload is returned when an uploaded file was downloaded but it's corrupted.
var ErrDownloadingCorruptedUpload = errors.New("upload: downloaded corrupted upload")

// ErrFileCorrupted is returned when the repository gets a file that is corrupted.
var ErrFileCorrupted = errors.New("upload: file corrupted")

// ErrFileDoesNotExist is returned when file is not in the store.
var ErrFileDoesNotExist = errors.New("upload: file does not exist")
