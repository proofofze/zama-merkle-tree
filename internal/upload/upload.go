package upload

// Upload contains all the information about an upload.
type Upload struct {
	Index int
	Name  string
	Data  []byte
}
