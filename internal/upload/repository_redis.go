package upload

import (
	"context"
	"fmt"
	"strconv"

	"github.com/juju/errors"
	"github.com/redis/go-redis/v9"
)

// Repository stores and gets uploads.
type Repository interface {
	// SetMany stores uploads.
	SetMany(ctx context.Context, uploads []*Upload) error
	// GetByIndex gets an upload by its index.
	GetByIndex(ctx context.Context, index int) (*Upload, error)
	// GetAll gets all uploads on redis.
	GetAll(ctx context.Context) ([]*Upload, error)
	// Flush flushes all keys from redis.
	Flush(ctx context.Context) error
}

// RedisRepository is an implementation of Repository using redis.
type RedisRepository struct {
	redisClient redis.Cmdable
}

// NewRedisRepository instantiates a new RedisRepository.
func NewRedisRepository(redisClient redis.Cmdable) *RedisRepository {
	return &RedisRepository{
		redisClient: redisClient,
	}
}

// SetMany stores uploads.
func (r *RedisRepository) SetMany(ctx context.Context, uploads []*Upload) error {
	pipe := r.redisClient.Pipeline()

	for _, upload := range uploads {
		pipe.HMSet(
			ctx,
			strconv.Itoa(upload.Index),
			nameKey,
			upload.Name,
			dataKey,
			string(upload.Data),
		)
	}

	_, err := pipe.Exec(ctx)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

// GetByIndex gets an upload by its index.
func (r *RedisRepository) GetByIndex(ctx context.Context, index int) (*Upload, error) {
	res, err := r.redisClient.HGetAll(ctx, strconv.Itoa(index)).Result()
	if err != nil {
		return nil, errors.Trace(err)
	}

	if len(res) == 0 {
		return nil, ErrFileDoesNotExist
	}

	name, ok := res[nameKey]
	if !ok {
		return nil, ErrFileCorrupted
	}

	data, ok := res[dataKey]
	if !ok {
		return nil, ErrFileCorrupted
	}

	return &Upload{
		Index: index,
		Name:  name,
		Data:  []byte(data),
	}, nil
}

// GetAll gets all uploads on redis.
func (r *RedisRepository) GetAll(ctx context.Context) ([]*Upload, error) {
	keys, err := r.redisClient.Keys(ctx, allKeys).Result()
	if err != nil {
		return nil, errors.Trace(err)
	}

	pipe := r.redisClient.Pipeline()

	for _, key := range keys {
		err := pipe.HMGet(
			ctx,
			key,
			nameKey,
			dataKey,
		).Err()
		if err != nil {
			return nil, errors.Trace(err)
		}
	}

	cmds, err := pipe.Exec(ctx)
	if err != nil {
		return nil, errors.Trace(err)
	}

	uploads := make([]*Upload, 0, len(keys))
	for i, c := range cmds {
		results := c.(*redis.SliceCmd).Val()

		if len(results) != 2 {
			return nil, errors.Trace(ErrDownloadingCorruptedUpload)
		}

		index, err := strconv.Atoi(keys[i])
		if err != nil {
			return nil, errors.Trace(err)
		}

		uploads = append(uploads, &Upload{
			Index: index,
			Name:  fmt.Sprintf("%v", results[0]),
			Data:  []byte(fmt.Sprintf("%v", results[1])),
		})
	}

	return uploads, nil
}

// Flush flushes all keys from redis.
func (r *RedisRepository) Flush(ctx context.Context) error {
	return errors.Trace(r.redisClient.FlushAll(ctx).Err())
}

const (
	nameKey = "name"
	dataKey = "data"
	allKeys = "*"
)
