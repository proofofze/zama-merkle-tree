package upload

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUploader_UploadMany(t *testing.T) {
	repo := &mockRepo{storage: make(map[int]*Upload)}

	t.Run("success", func(t *testing.T) {
		repo.storage[10] = nil
		uploads := []*Upload{
			{
				Index: 0,
				Name:  "ho.txt",
				Data:  []byte("hohoho"),
			},
		}

		uploader := NewUploader(repo)

		err := uploader.UploadMany(context.Background(), uploads)
		assert.Nil(t, err)
		assert.Equal(t, repo.storage, map[int]*Upload{
			0: {
				Index: 0,
				Name:  "ho.txt",
				Data:  []byte("hohoho"),
			},
		})
	})

	t.Run("error", func(t *testing.T) {
		repo.storage[10] = nil

		uploader := NewUploader(repo)

		err := uploader.UploadMany(context.Background(), nil)
		assert.ErrorIs(t, err, assert.AnError)
		assert.Len(t, repo.storage, 0)
	})
}

func TestUploader_Download(t *testing.T) {
	repo := &mockRepo{storage: make(map[int]*Upload)}

	t.Run("success", func(t *testing.T) {
		repo.storage[0] = &Upload{
			Index: 0,
			Name:  "ho.txt",
			Data:  []byte("hohoho"),
		}

		uploader := NewUploader(repo)

		name, data, err := uploader.Download(context.Background(), 0)
		assert.Nil(t, err)
		assert.Equal(t, name, "ho.txt")
		assert.Equal(t, data, []byte("hohoho"))
	})

	t.Run("error", func(t *testing.T) {
		uploader := NewUploader(repo)

		_, _, err := uploader.Download(context.Background(), 10)
		assert.ErrorIs(t, err, assert.AnError)
	})
}

type mockRepo struct {
	storage map[int]*Upload
}

func (m *mockRepo) SetMany(_ context.Context, uploads []*Upload) error {
	if uploads == nil {
		return assert.AnError
	}

	for _, u := range uploads {
		m.storage[u.Index] = u
	}
	return nil
}

func (m *mockRepo) GetByIndex(_ context.Context, index int) (*Upload, error) {
	u, ok := m.storage[index]
	if !ok {
		return nil, assert.AnError
	}

	return u, nil
}

func (m *mockRepo) GetAll(_ context.Context) ([]*Upload, error) {
	uploads := make([]*Upload, 0, len(m.storage))
	for _, u := range m.storage {
		uploads = append(uploads, u)
	}
	return uploads, nil
}

func (m *mockRepo) Flush(_ context.Context) error {
	m.storage = map[int]*Upload{}
	return nil
}
