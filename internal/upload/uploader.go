package upload

import (
	"context"
	"sort"

	"github.com/juju/errors"
)

// Uploader uploads uploads.
type Uploader struct {
	repository Repository
}

// NewUploader instantiates a new Uploader.
func NewUploader(repository Repository) *Uploader {
	return &Uploader{
		repository: repository,
	}
}

// UploadMany pushes uploads.
func (u *Uploader) UploadMany(ctx context.Context, uploads []*Upload) error {
	err := u.repository.Flush(ctx)
	if err != nil {
		return errors.Trace(err)
	}

	return errors.Trace(u.repository.SetMany(ctx, uploads))
}

// Download fetches uploaded files.
func (u *Uploader) Download(ctx context.Context, index int) (string, []byte, error) {
	upload, err := u.repository.GetByIndex(ctx, index)
	if err != nil {
		return "", nil, errors.Trace(err)
	}

	return upload.Name, upload.Data, nil
}

// DownloadAll gets all files.
func (u *Uploader) DownloadAll(ctx context.Context) ([]*Upload, error) {
	uploads, err := u.repository.GetAll(ctx)
	if err != nil {
		return nil, errors.Trace(err)
	}

	sort.Slice(uploads, func(i, j int) bool {
		return uploads[i].Index < uploads[j].Index
	})

	return uploads, nil
}
