package upload

import (
	"context"
	"testing"

	"github.com/go-redis/redismock/v9"
	"github.com/stretchr/testify/assert"
)

func TestRedisRepository_SetMany(t *testing.T) {
	db, mock := redismock.NewClientMock()
	uploads := []*Upload{
		{
			Index: 0,
			Name:  "first.txt",
			Data:  []byte("first"),
		},
	}

	t.Run("success", func(t *testing.T) {
		mock.ExpectHMSet(
			"0",
			nameKey,
			"first.txt",
			dataKey,
			"first",
		).SetVal(true)

		repo := NewRedisRepository(db)

		err := repo.SetMany(context.Background(), uploads)
		assert.Nil(t, err)
	})

	t.Run("error", func(t *testing.T) {
		mock.ExpectHMSet(
			"0",
			nameKey,
			"first.txt",
			dataKey,
			"first",
		).SetErr(assert.AnError)

		repo := NewRedisRepository(db)

		err := repo.SetMany(context.Background(), uploads)
		assert.ErrorIs(t, err, assert.AnError)
	})
}

func TestRedisRepository_GetByIndex(t *testing.T) {
	db, mock := redismock.NewClientMock()
	expectedUpload := &Upload{
		Index: 0,
		Name:  "file.txt",
		Data:  []byte("hithere"),
	}

	t.Run("success", func(t *testing.T) {
		mock.
			ExpectHGetAll("0").
			SetVal(map[string]string{
				nameKey: "file.txt",
				dataKey: "hithere",
			})

		repo := NewRedisRepository(db)

		u, err := repo.GetByIndex(context.Background(), 0)
		assert.Nil(t, err)
		assert.Equal(t, expectedUpload, u)
	})

	t.Run("error", func(t *testing.T) {
		mock.
			ExpectHGetAll("0").
			SetErr(assert.AnError)

		repo := NewRedisRepository(db)

		_, err := repo.GetByIndex(context.Background(), 0)
		assert.ErrorIs(t, err, assert.AnError)
	})

	t.Run("file_doesnt_exist", func(t *testing.T) {
		mock.
			ExpectHGetAll("0").
			SetVal(nil)

		repo := NewRedisRepository(db)

		_, err := repo.GetByIndex(context.Background(), 0)
		assert.ErrorIs(t, err, ErrFileDoesNotExist)
	})

	t.Run("name_corrupted", func(t *testing.T) {
		mock.
			ExpectHGetAll("0").
			SetVal(map[string]string{
				"sup": "file.txt",
			})

		repo := NewRedisRepository(db)

		_, err := repo.GetByIndex(context.Background(), 0)
		assert.ErrorIs(t, err, ErrFileCorrupted)
	})

	t.Run("file_corrupted", func(t *testing.T) {
		mock.
			ExpectHGetAll("0").
			SetVal(map[string]string{
				nameKey: "file.txt",
				"sup":   "hithere",
			})

		repo := NewRedisRepository(db)

		_, err := repo.GetByIndex(context.Background(), 0)
		assert.ErrorIs(t, err, ErrFileCorrupted)
	})
}

func TestRedisRepository_GetAll(t *testing.T) {
	db, mock := redismock.NewClientMock()
	expectedUpload := &Upload{
		Index: 0,
		Name:  "file.txt",
		Data:  []byte("hithere"),
	}

	t.Run("success", func(t *testing.T) {
		mock.ExpectKeys(allKeys).SetVal([]string{"0"})
		mock.ExpectHMGet("0", nameKey, dataKey).SetVal([]interface{}{
			"file.txt",
			"hithere",
		})

		repo := NewRedisRepository(db)

		u, err := repo.GetAll(context.Background())
		assert.Nil(t, err)
		assert.Len(t, u, 1)
		assert.Equal(t, expectedUpload, u[0])
	})

	t.Run("error", func(t *testing.T) {
		mock.ExpectKeys(allKeys).SetErr(assert.AnError)

		repo := NewRedisRepository(db)

		_, err := repo.GetAll(context.Background())
		assert.ErrorIs(t, err, assert.AnError)
	})

	t.Run("corrupted_file", func(t *testing.T) {
		mock.ExpectKeys(allKeys).SetVal([]string{"0"})
		mock.ExpectHMGet("0", nameKey, dataKey).SetVal([]interface{}{
			"corrupted",
		})

		repo := NewRedisRepository(db)

		_, err := repo.GetAll(context.Background())
		assert.ErrorIs(t, err, ErrDownloadingCorruptedUpload)
	})
}
