package file

import (
	"fmt"
	"os"
	"sort"

	"github.com/juju/errors"
)

// Manager manages files and its directories.
type Manager struct{}

// NewManager instantiates a new Manager.
func NewManager() *Manager {
	return &Manager{}
}

// GetAll gets all files inside a directory.
func (m *Manager) GetAll(directory string) ([]*File, error) {
	dirs, err := os.ReadDir(directory)
	if err != nil {
		return nil, errors.Trace(err)
	}

	files := make([]*File, 0, len(dirs))
	for i, dir := range dirs {
		fileName := dir.Name()
		path := fmt.Sprintf("%s/%s", directory, dir.Name())

		data, err := os.ReadFile(path)
		if err != nil {
			return nil, errors.Trace(err)
		}

		files = append(files, &File{
			Index: i,
			Name:  fileName,
			Data:  data,
		})
	}

	sort.Slice(files, func(i, j int) bool {
		b1 := []byte(files[i].Name)
		b2 := []byte(files[j].Name)

		if len(b1) < len(b2) {
			return true
		}

		if len(b2) < len(b1) {
			return false
		}

		return files[i].Name < files[j].Name
	})

	for i, f := range files {
		f.Index = i
	}

	return files, nil
}

// RemoveAll removes all files inside a directory.
func (m *Manager) RemoveAll(dir string) error {
	err := os.RemoveAll(dir)
	if err != nil {
		return errors.Trace(err)
	}

	return errors.Trace(os.MkdirAll(dir, os.ModePerm))
}

// DirExists checks if a directory exists.
func (m *Manager) DirExists(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return errors.Trace(err)
	}

	return nil
}

// Create creates a new file inside a directory.
func (m *Manager) Create(dir, name string, data []byte) error {
	f, err := os.Create(fmt.Sprintf("%s/%s", dir, name))
	if err != nil {
		return errors.Trace(err)
	}

	defer f.Close()

	_, err = f.Write(data)
	if err != nil {
		return errors.Trace(err)
	}

	return nil
}
