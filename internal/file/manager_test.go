package file

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestManager(t *testing.T) {
	manager := NewManager()
	dir := "./files"

	err := os.MkdirAll(dir, os.ModePerm)
	assert.Nil(t, err)

	err = manager.Create(dir, "test.txt", []byte("hithere"))
	assert.Nil(t, err)

	files, err := manager.GetAll(dir)
	assert.Nil(t, err)
	assert.Len(t, files, 1)
	assert.Equal(t, &File{
		Index: 0,
		Name:  "test.txt",
		Data:  []byte("hithere"),
	}, files[0])

	err = manager.RemoveAll(dir)
	assert.Nil(t, err)

	files, err = manager.GetAll(dir)
	assert.Nil(t, err)
	assert.Len(t, files, 0)

	err = os.RemoveAll(dir)
	assert.Nil(t, err)
}
