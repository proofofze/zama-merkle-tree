package file

// File contains information about a file.
type File struct {
	Index int
	Name  string
	Data  []byte
}
