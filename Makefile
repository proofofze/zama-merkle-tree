.PHONY: test fmt vet proto

PROJECT_NAME := zama-merkle-tree
FOLDER_BIN = $(CURDIR)/bin
GO_TEST = go test ./...
GO_FILES = $(shell go list ./... | grep -v /vendor/)
COMMIT_SHA = $(shell git rev-parse --short HEAD)
TAG_COMMIT_SHA = $(PROJECT_NAME):$(COMMIT_SHA)
TAG_LATEST = $(PROJECT_NAME):latest

server-start:
	go run cmd/server/main.go -redisAuth "R€d1s!"

server-build:
	go build -o $(FOLDER_BIN)/server $(CURDIR)/cmd/server/main.go

client-start:
	go run cmd/client/main.go

client-build:
	go build -o $(FOLDER_BIN)/client $(CURDIR)/cmd/client/main.go

fbuild:
	go run cmd/fbuilder/main.go

test:
	$(GO_TEST) -coverprofile cover.out

test-race:
	$(GO_TEST) -race

lint:
	golangci-lint run

fmt:
	go fmt $(GO_FILES)

vet:
	go vet $(GO_FILES)

test-all: fmt vet test-race

mod:
	go mod tidy

generate:
	go generate ./...

proto:
	 protoc --proto_path=proto proto/server/server.proto --go_out=. --go-grpc_out=.

up:
	docker-compose up -d

down:
	docker-compose stop