FROM golang:1.21 as builder

WORKDIR /app

# Get dependencies
COPY go.mod ./
RUN go mod download

# Copying all the files to build the binaries
COPY . .

# Build it for linux OS
RUN CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -a -o ./bin/server ./cmd/server/main.go

FROM alpine

# Exposing server port
EXPOSE 8000

COPY --from=builder /app/bin/server /server

CMD /server -serverAddr $SERVER_ADDRESS -redisAddr $REDIS_ADDRESS -redisAuth $REDIS_AUTH