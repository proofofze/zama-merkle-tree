# Zama Merkle Tree Challenge
## Overview
This project aims to create a client, a server and a reliable way to upload and download small files
between them.

The client has the option to upload a large set of potentially small files {F0, F1, …, Fn} to
a server and then delete its local copies.

The client can also, later, download an arbitrary file from the server and make sure that the file is
correct and is not corrupted in any way (in transport, tampered with by the server, etc.) by making
use of a merkle tree implementation.

Moreover, the client computes a single Merkle tree root hash and keep it on its disk after uploading 
the files to the server and deleting its local copies so it can request the i-th file Fi and a Merkle
proof Pi for it from the server. 
The client uses the proof and compares the resulting root hash with the one it persisted before deleting
the files - if they match, file is correct.

## Elements of the system
The system is composed by 3 different elements: a client, a server and a redis instance. They can be
deployed on different machines and still work perfectly as they communicate through TCP.

Despite the ability to deploy all elements in different machines, the system only supports one client,
one server and one redis. As a future work, this could be improved.

### Client
The client is in fact a REPL (Read-Evaluate-Print Loop) implementation. It allows a few commands so the
user can chose the directory where the files are, upload and download them through the command line.

The client also has a command argument when starting up. It's `serverAddr` and it's used to set the
server address so the client can communicate with it. This argument is optional and, if not set, the
default value is `127.0.0.1:8000`.

#### Commands

* `DIR <dir>` Pick a directory to upload all its files to the server. Downloaded files will also be added there.
* `UPLOAD` Uploads all files that are inside the picked directory. After, a root hash of the merkle tree computed from the uploaded files is saved. You can check the value with the ROOTHASH command.
* `DOWNLOAD <fileIndex>` Download the uploaded file to picked directory with the index <fileIndex> and validates it using the root hash saved during the upload and the merkle proof sent from the DOWNLOAD response.
* `ROOTHASH` Shows the hash of the merkle tree root node computed during the upload.
* `HELP` Shows all available commands.
* `QUIT` Exit the REPL cleanly. A message to stderr may be output.

For simplicity, all keys and values are simple ASCII strings delimited by whitespace. No quoting is needed.
All errors are output to stderr.
Commands are case-insensitive.
As this is a simple command line program, there is only one “client” at a time for now.

### Server
The server is a gRPC server that contains two endpoints: upload and download.
gRPC was preferred over REST for its performance and simplicity.

The client also has a few command arguments when starting up. They are all optional since they have default
values if they are not set.
* `serverAddr` - Sets the address of the server. Ideally, it only has to use the port (i.e. `serverAddress=":8000"`). The default value is `127.0.0.1:8000`;
* `redisAddr` - Sets the redis address for the server to connect to it. The default value is `127.0.0.1:6379`;
* `redisAuth` - Sets the redis authentication (if it has one). No default value.
* It's `serverAddr` and it's used to set the
server address so the client can communicate with it. This argument is optional and, if not set, the
default value is `127.0.0.1:8000`.

### Redis
The server uses a redis instance to store the uploaded files. Redis was picked due to the fact that
this project is simple and it "does the job". There's no need to use a Relational Database and maintain schemas.

### gRPC communication
The communication between the client and server is done via gRPC (the messages are serialized using protobufs).
You can check the messages [here](proto/server/server.proto).

* Upload - This endpoint is a stream endpoint. Whoever consumes it will send all files over this stream.
* Download - On the other side, this one is a unary.

## Flow
<img src="./static/flow.png" width="500" height="550">

1. The client starts by fetching all the files in a provided directory and streams them to the server
through the gRPC Upload endpoint.
2. 
   1. The server saves the files on redis. 
   2. After, it computes a merkle tree with all the files and saves it in memory.
3. After getting a successful response, the client computes a merkle tree from the uploaded files and
save the root hash in memory. It also deletes all files from that directory.
4. Whenever the client wishes, it can request a specific file from the server by its order id (index).
To do so, the client calls the gRPC Download endpoint with the requested index to the server.
5. The server gets the respective file from redis and builds the respective Merkle proof. The file and
proof are then sent back to the client.
6. The client validates the file with the merkle proof to check if the file is corrupted or not.

## How to run the system locally
### Installation Requirements
Install:
* Go 1.21.* (other versions work as well) - `brew install go`
* Make - `brew install make`
* Docker Compose - `brew install docker-compose`
* redis-cli - `brew install redis`
* Golangci-lint (optional - for development only) - `brew install golangci-lint`

(Note that this is for MacOS. Do the respective on Linux or Windows)

### Start up the system (using `docker-compose`)
To start up the server and redis instance, run the following: `make up`

To stop it, do: `make down`

To run the repl client, run: `make client-start`

### Run the server with go
To run the server with go, do `go run cmd/server/main.go -redisAuth "R€d1s!"`.

You can also build the server binary with `make server-build` and run it with `./bin/server -redisAuth "R€d1s!"`.
(Note that redis can only be ran with docker-compose. Or if you install a redis instance locally.)

To start the client with go, just run `go run cmd/client/main.go`.

### Other important commands
```
# If you need to download go dependencies.
make mod

# Run tests
make test

# Run tests
make test-all

# Run tests with coverage
make test-cover

# Run linter
make lint

# Generate protobufs
make proto

# Generate mock grpc server
make generate
```

## Updating files on redis
If you wish to update/delete/add files to redis to force an invalid or corrupted file, do the following:
```
# connect to redis with default configs
redis-cli

# authenticate
auth <PASS>

# edit existing file
HMSET <FILE_INDEX> name <FILE_NAME> data <NEW_FILE_DATA>
```

Now, try to download that file from the clint and watch it saying that it is corrupted.

## Design decisions and future work
### Why redis
As stated above, redis was picked because of its simplicity despite its downsides.
I didn't see the advantage of using a relational database when it would just introduce more complexity
to the system (due to the schema management).

Since this system only handles one server, one client and one repository at the same time and is simple ,
redis (being a key-value in memory database) is more than capable of handling it.

However, there's a known issue here: if, by any reason, redis instance goes down, it loses all the uploaded
files and there's no way of getting it unless the client uploads it again (which might not happen because
it deleted all files after the first upload).

This can be resolved by adding a redis cluster or simply migrating to a relational or document database.
Or even creating a P2P network of servers, and each server has its own redis instance. If a redis instance
goes out of service, the respective service can request all the uploads from another.
Due to the time limitations and simplicity of the challenge, this was not needed, of course.

### Fault-proof server
The server, on the other side, can go out of service that, when it comes back, it will fetch all the
uploaded files from redis and build the merkle tree again. With this, it's always assured that a client
can download the files, even if the server goes out and comes back up again.

### Sending files over a stream
Imagine sending all files over a unary or a REST API, one by one. It would take forever.
Since we might have thousands of (small) files, sending them from the client to the server can be a
cumbersome task and it's probably where the biggest bottleneck of the system is.

In order to reduce the files upload time, we could either send them in parallel (but there's a limit in golang
for how many file descriptors we might have at the same time) or send them over a stream.

I've picked the stream option since we can just send all files, one by one, without waiting for a response
from the server and, only after the client streams all the files, it waits for the response. It reduces
drastically the upload time.
Also, it's easier to handle synchronization between server and client with a gRPC stream.

### Merkle Tree Computation in parallel
We might have thousands of files that need to be computed in the merkle tree. This might also be an 
heavy task and it can be optimized by building the merkle tree's nodes in parallel.

For instance, we can have multiple go routines building the hashes of the first level nodes (leaf nodes).
After, the same go routines can build the next level hashes, and so on.

Still, this system's implementation does not do that due to time limitatiosn but it will be added
to the future work task list.

### Testing
Despite having a lot of unit tests, there's still a lot of space to improvement. Whether it's for
adding integration/end2end tests (which there are none) or just improving/completing the ones that
already exists. But due to time limitations, this will also be a topic fo the future

### Future work
- [ ] Peer-to-peer network implementation;
- [ ] Build merkle tree in parallel;
- [ ] Add a redis cluster;
- [ ] Add integration tests;
- [ ] Add alarmistic and monitoring.