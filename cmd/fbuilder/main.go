package main

import (
	"fmt"
	"math/rand"
	"os"

	"github.com/juju/errors"
)

func main() {
	for i := 0; i < 10000; i++ {
		if err := createFile(i); err != nil {
			panic(err)
		}
	}
}

func createFile(i int) error {
	f, err := os.Create(fmt.Sprintf("files/file-%d.txt", i))
	if err != nil {
		return errors.Trace(err)
	}

	defer f.Close()

	_, err = f.WriteString(randSeq(1024 * 100))

	if err != nil {
		return errors.Trace(err)
	}

	return nil
}

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		//nolint
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

var letters = []rune("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJ.KLMNOPQRSTUVWXYZ1234567890!#$%&/()")
