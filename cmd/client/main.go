package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"lab/zama-merkle-tree/internal/file"
	"lab/zama-merkle-tree/internal/grpc/client"
	"lab/zama-merkle-tree/internal/hash"
	"lab/zama-merkle-tree/internal/merkle"
	"lab/zama-merkle-tree/internal/repl"
	"lab/zama-merkle-tree/proto/server/pb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	logger := log.New(os.Stderr, "", 0)

	serverAddr := flag.String("serverAddr", "127.0.0.1:8000", "address of the upload server")
	flag.Parse()

	if serverAddr == nil || *serverAddr == "" {
		log.Fatal("invalid argument: serverAddr")
	}

	conn, err := grpc.Dial(*serverAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()

	uploaderClient := pb.NewFileUploaderClient(conn)
	grpcClient := client.New(uploaderClient)

	sha256Hasher := hash.NewSHA256Hasher()
	merkleTree := merkle.NewTree(sha256Hasher, 10)

	fileManager := file.NewManager()

	r := repl.NewREPL(logger, grpcClient, merkleTree, fileManager)
	if err := r.Start(); err != nil {
		logger.Fatal(err)
	}
	fmt.Println("Good bye and be safe!")
}
