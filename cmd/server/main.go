package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"lab/zama-merkle-tree/internal/grpc/server"
	"lab/zama-merkle-tree/internal/hash"
	"lab/zama-merkle-tree/internal/merkle"
	"lab/zama-merkle-tree/internal/upload"
	"lab/zama-merkle-tree/proto/server/pb"

	"github.com/redis/go-redis/v9"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	serverAddr := flag.String("serverAddr", "127.0.0.1:8000", "address of the upload server")
	redisAddr := flag.String("redisAddr", "127.0.0.1:6379", "address of the redis instance")
	redisAuth := flag.String("redisAuth", "R€d1s!", "password of the redis instance")
	flag.Parse()

	if serverAddr == nil || *serverAddr == "" {
		log.Fatal("invalid argument: serverAddr")
	}
	if redisAddr == nil || *redisAddr == "" {
		log.Fatal("invalid argument: redisAddr")
	}
	if redisAuth == nil || *redisAuth == "" {
		log.Fatal("invalid argument: redisAuth")
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     *redisAddr,
		Password: *redisAuth,
	})

	logger := log.New(os.Stderr, "", 0)

	listener, err := net.Listen("tcp", *serverAddr)
	if err != nil {
		panic(err)
	}

	sha256Hasher := hash.NewSHA256Hasher()
	merkleTree := merkle.NewTree(sha256Hasher, 10)

	redisRepo := upload.NewRedisRepository(rdb)
	uploader := upload.NewUploader(redisRepo)

	grpcServer := server.NewServer(uploader, merkleTree)

	loggingMiddleware := server.NewLoggingMiddleware(logger)

	// TODO: add context middleware to force a timeout
	s := grpc.NewServer(
		grpc.StreamInterceptor(loggingMiddleware.StreamInterceptor),
		grpc.UnaryInterceptor(loggingMiddleware.UnaryInterceptor),
	)
	pb.RegisterFileUploaderServer(s, grpcServer)
	reflection.Register(s)

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		fmt.Println("Listening on", *serverAddr)
		if err := s.Serve(listener); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	<-done
	log.Print("Server has stopped")
	s.GracefulStop()
	log.Print("Server stopped properly")
}
